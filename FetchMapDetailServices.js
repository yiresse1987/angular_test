/**
 * Created by octal-mac on 07/12/15.
 */

mapapp.service('Mapservice', function ($http) {


    $http.defaults.headers.post["Content-Type"] = "text/plain";

    this.GetMapDetail = function (mapArr) {
        var request = $http({
            method: "POST",
            url: "https://development.sixthcontinent.com/webapi/getstoremarkers?access_token=M2EzMDM3ZDM2NWU4ZDE0MzNlZDRkOTczYjcyMTllN2EyZmI4ZTc1MmJmMzA5ZTIwYTAwNTVjOWYwYWQyNzllZg&session_id=9732",
            data: mapArr
        });
        return request;
    }

    this.GetMapOnBasisOfSelection = function(maparr)
    {
        var request = $http({
            method: 'POST',
            url: "https://development.sixthcontinent.com/webapi/getstoremarkers?access_token=M2EzMDM3ZDM2NWU4ZDE0MzNlZDRkOTczYjcyMTllN2EyZmI4ZTc1MmJmMzA5ZTIwYTAwNTVjOWYwYWQyNzllZg&session_id=9732",
            data:maparr
        });
        return request;
    }
});