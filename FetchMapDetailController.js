/**
 * Created by octal-mac on 07/12/15.
 */
mapapp.controller('mapcontroller', function ($scope, Mapservice) {


    //-------------------------------- Page load start ---------------------------------------------------


    GetMapDetail();

    //-------------------------------- Page load end ---------------------------------------------------

    //---------------------------- set all marker on map start -----------------------------------------------

    //<summary>
    // Written by Bhawani Singh on 7th Dec 2015
    // This code will return list of all locations and mark them on map.
    //</summary>

    function GetMapDetail() {
        var mapinfoArr={"reqObj":{"category":[""],"latitude":45.4667,"longitude":9.1833,"type":"store","query":"","limit_distance":"","marker_type":""}};
        var promiseGet = Mapservice.GetMapDetail(mapinfoArr);
        promiseGet.then(function (pl) {

                $scope.MapDataList = pl.data;


                var myLatlng = new google.maps.LatLng(pl.data.result[0].latitude, pl.data.result[0].longitude);
                var mapOptions = {
                    center: myLatlng,
                    zoom: 3,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("map"), mapOptions);
                // Geo Location /
                navigator.geolocation.getCurrentPosition(function(pos) {
                    map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
                    var myLocation = new google.maps.Marker({
                        position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
                        map: map,
                        animation: google.maps.Animation.DROP,
                        title: "My Location"
                    });
                });
                $scope.map = map;
                // Additional Markers //
                $scope.markers = [];
                var infoWindow = new google.maps.InfoWindow();
                var createMarker = function (info){

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(info.lat, info.long),
                        map: $scope.map,
                        animation: google.maps.Animation.DROP,
                        title: info.city
                    });
                    marker.content = '<div class="infoWindowContent">' + info.desc + '</div>';
                    google.maps.event.addListener(marker, 'click', function(){
                        infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                        infoWindow.open($scope.map, marker);
                    });
                    $scope.markers.push(marker);
                }

                for (i = 0; i < $scope.MapDataList.result.length; i++){
                    var mapinfo = {
                        city : $scope.MapDataList.result[i].business_city,
                        desc : $scope.MapDataList.result[i].business_name,
                        lat : $scope.MapDataList.result[i].latitude,
                        long : $scope.MapDataList.result[i].longitude

                    }
                    createMarker(mapinfo);
                }

            },
            function (pl) {
                $scope.error = "An Error has occured while Loading users! " + pl.ExceptionMessage;
            });
    }

//---------------------------- set all marker on map end --------------------------------------------------------------


//---------------------------- set marker according selected location start -------------------------------------------

    //<summary>
    // Written by Abhinav Walia on 7th Dec 2015
    // This code will return list of selected location and mark them on map.
    //</summary>

    function GetMapData(maparr) {


        var promiseGet = Mapservice.GetMapOnBasisOfSelection(maparr);
        promiseGet.then(function (pl) {
                //console.log(angular.toJson(pl.data));
                if(pl.data.result.length == 0) {
                    while($scope.markers.length){
                        $scope.markers.pop().setMap(null);
                    }
                    alert("Data not available for this location.");

                }
                else {
                    var myLatlng = new google.maps.LatLng(pl.data.result[0].latitude, pl.data.result[0].longitude);
                    var mapOptions = {
                        center: myLatlng,
                        zoom: 3,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
                    // Geo Location /
                    navigator.geolocation.getCurrentPosition(function (pos) {
                        map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
                        var myLocation = new google.maps.Marker({
                            position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
                            map: map,
                            animation: google.maps.Animation.DROP,
                            title: "My Location"
                        });
                    });
                    $scope.map = map;
                    // Additional Markers //
                    $scope.markers = [];
                    var infoWindow = new google.maps.InfoWindow();
                    var createMarker = function (info) {

                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(info.lat, info.long),
                            map: $scope.map,
                            animation: google.maps.Animation.DROP,
                            title: info.city
                        });
                        marker.content = '<div class="infoWindowContent">' + info.desc + '</div>';
                        google.maps.event.addListener(marker, 'click', function () {
                            infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                            infoWindow.open($scope.map, marker);
                        });
                        $scope.markers.push(marker);
                    }

                    for (i = 0; i < pl.data.result.length; i++) {
                        var mapinfo = {
                            city: pl.data.result[i].business_city,
                            desc: pl.data.result[i].business_name,
                            lat: pl.data.result[i].latitude,
                            long: pl.data.result[i].longitude

                        }
                        createMarker(mapinfo);
                    }
                }

            },
            function (pl) {
                $scope.error = "An Error has occured while Loading users! " + pl.ExceptionMessage;
            });
    }

    //---------------------------- set marker according selected location end -------------------------------------------


    //---------------------------- search click code start --------------------------------------------------------------

    //<summary>
    // Written by Abhinav Walia on 7th Dec 2015
    // This code fire on search button click.
    //</summary>

    $scope.SearchLocation = function(){
        if(typeof($scope.location) === "undefined"){
            alert('Please select a location to search.');
        } else {

            var res = $scope.location.split(",");
            var Lat=(res[0]);
            var Long=(res[1]);
            var MapArrInfo={"reqObj":{"category":[""],"latitude":Lat,"longitude":Long,"type":"store","query":"","limit_distance":"","marker_type":"","keywords":{}}};
            GetMapData(MapArrInfo);
        }
    };

    //---------------------------- search click code end --------------------------------------------------------------

});